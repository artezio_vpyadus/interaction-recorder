import React from 'react';
import PropTypes from 'prop-types';
import { DropdownButton, MenuItem } from 'react-bootstrap';

function Dropdown(props) {
    const hoverStyle = {color: '#262626',
        textDecoration: 'none',
        backgroundColor: '#f5f5f5'
    };
    return (
        <DropdownButton id={props.id}
                        open={props.open}
                        title={props.selected}
                        selected={props.selected}
                        onToggle={props.onToggle}
                        onSelect={props.onSelect}
        >
            {props.options.map((opt, i) => {
                return (
                    <MenuItem eventKey={opt}
                              name={opt}
                              active={opt === props.selected}
                              style={opt === props.hover ? hoverStyle : null}
                              key={i}
                              onMouseEnter={props.onMouseEnter}
                    >
                        {opt}
                    </MenuItem>
                )
            })}
        </DropdownButton>
    )
}

Dropdown.propTypes = {
    id: PropTypes.string.isRequired,
    open: PropTypes.bool,
    options: PropTypes.array,
    selected: PropTypes.string,
    hover: PropTypes.string,
    onToggle: PropTypes.func,
    onSelect: PropTypes.func,
    onMouseEnter: PropTypes.func
};

Dropdown.defaultProps = {
    id: '',
    open: false,
    options: [],
    selected: '',
    disabled: false,
    hover: '',
    onToggle: null,
    onSelect: null,
    onMouseEnter: null
};


export default Dropdown;
