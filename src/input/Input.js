import React from 'react';
import PropTypes from 'prop-types';

function Input(props) {
    return (
        <input type={props.type}
               name={props.name}
               className="form-control"
               value={props.value}
               onChange={props.onChange}
        />
    )
}

Input.propTypes = {
    type: PropTypes.string,
    name: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func
};

Input.defaultProps = {
    type: 'text',
    name: '',
    value: '',
    onChange: null
};

export default Input;