import React from 'react';
import PropTypes from 'prop-types';
import { ButtonToolbar, ToggleButtonGroup, ToggleButton } from 'react-bootstrap';
import classNames from 'classnames';

function Pills(props) {
    return (
        <ButtonToolbar>
            <ToggleButtonGroup type="radio"
                               name="options"
                               defaultValue={props.selected}
                               onChange={props.onChange}
            >
                {props.options.map((opt, i) => {
                    return (
                        <ToggleButton value={opt}
                                      className={classNames({'btn-primary': opt === props.selected})}
                                      name={opt}
                                      key={i}
                        >
                            {opt}
                        </ToggleButton>
                    )
                })}
            </ToggleButtonGroup>
        </ButtonToolbar>
    );
}

Pills.propTypes = {
    options: PropTypes.array,
    selected: PropTypes.string,
    onChange: PropTypes.func
};

export default Pills;
