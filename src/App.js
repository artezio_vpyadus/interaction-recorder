import React, { Component } from 'react';
import Recorder from './record/Recorder';
import Input from './input/Input';
import Button from './button/Button';
import Dropdown from './dropdown/Dropdown';
import PlayerProgressBar from './progress/PlayerProgressBar';
import Pills from './pills/Pills';

class App extends Component {

    /**
     * Объект состояния приложения
     * @type {{inputName: Object, inputSecondName: Object,
     * inputAge: Object, dropdownGender: Object,
     * pillsSelected: Object, playbackCurrentTime: Number,
     * recording: boolean, playing: boolean}}
     * состояние поля ввода имени, состояние поля ввода фамилии,
     * состояние поля ввода возраста, состояние выпадающего списка выбора пола,
     * состояние компонента выбора скорости возпроизведения,
     * текущее время воспроизведения, флаги записи и воспроизведения
     */
    state = {
        inputName: {value: ''},
        inputSecondName: {value: ''},
        inputAge: {value: ''},
        dropdownGender: {selected: 'Select Gender',
            open: false,
            hover: ''},
        pillsSpeed: {selected: ''},
        playbackCurrentTime: 0,
        recording: false,
        playing: false
    };
    /**
     * Объект, хранящий и воспроизводящий записанные действия пользователя
     * @type {Record}
     */
    savedSnapshots = null;
    /**
     * Объект, записывающий действия пользователя, когда нажата кнопка Запись
     * @type {Recorder}
     */
    recorder = null;

    /**
     * Массив вариантов выбора для выпадающего списка Gender
     * @type {Array}
     */
    GENDERS = ['Male', 'Female'];

    /**
     * Массив вариантов выбора для скорости воспроизведения
     * @type {Array}
     */
    SPEEDS = ['x0.5', 'x1', 'x2', 'x4', 'x8'];

    /**
     * Флаг активации полосы воспроизведения-перемотки
     * @type {boolean}
     */
    enableProgressBar = false;

    /**
     * Объект с методами изменения состояния приложения
     * в зависимости от действий пользователя в форме
     * @type {Object}
     */
    eventStateSetters = {
        change(prevState, name, eventInfo) {
            return {
                [name]: {
                    value: eventInfo.value
                }
            }
        },
        toggle(prevState, name, eventInfo) {
            return {
                [name]: {
                    selected: prevState[name].selected,
                    open: eventInfo.open,
                    hover: ''
                }
            }
        },
        select(prevState, name, eventInfo) {
            return {
                [name]: {
                    selected: eventInfo.selected,
                    open: eventInfo.open,
                    hover: ''
                }
            }
        },
        hover(prevState, name, eventInfo) {
            return {
                [name]: {
                    selected: prevState[name].selected,
                    open: eventInfo.open,
                    hover: eventInfo.hover
                }
            }
        }
    };

    /**
     * Начинает процесс записи действий пользователя
     */
    startRecording = () => {
        this.enableProgressBar = false;
        this.setState({recording: true});
        this.recorder = new Recorder();
        this.recorder.start(
            {
                name: 'inputName',
                eventInfo: {
                    eventType: 'change',
                    value: this.state.inputName.value
                }
            },
            {
                name: 'inputSecondName',
                eventInfo: {
                    eventType: 'change',
                    value: this.state.inputSecondName.value
                }
            },
            {
                name: 'inputAge',
                eventInfo: {
                    eventType: 'change',
                    value: this.state.inputAge.value
                }
            },
            {
                name: 'dropdownGender',
                eventInfo: {
                    eventType: 'select',
                    selected: this.state.dropdownGender.selected,
                    open: this.state.dropdownGender.open,
                    hover: this.state.dropdownGender.hover
                }
            }
        );
    };

    /**
     * Останавливает процесс записи или воспроизведения действий пользователя
     */
    stop = () => {
        if (this.state.recording) {
            this.savedSnapshots = this.recorder.stop();
            this.setState({
                recording: false,
                playbackCurrentTime: 0
            });
        }
        if (this.state.playing) {
            this.savedSnapshots.stop();
            this.setState({playing: false});
        }
    };

    /**
     * Функция-коллбэк для изменения состояния формы, передается объекту
     * с сохраненными действиями пользователя
     * @param {Object} snapshotState объект с сохраненным состоянием формы на текущий момент
     * @param {Number} currentTime текущий момент воспроизведения
     */
    playerCallBackFn = (snapshotState, currentTime) => {
        if (!snapshotState) {
            this.setState(prevState => {
                return {
                    playing: prevState.playing && currentTime < this.savedSnapshots.duration,
                    playbackCurrentTime: currentTime
                }
            })
        } else {
            const stateSetter = this.eventStateSetters[snapshotState.eventInfo.eventType];
            this.setState(prevState => {
                const newState = stateSetter(prevState, snapshotState.name, snapshotState.eventInfo);
                newState.playing = prevState.playing && currentTime < this.savedSnapshots.duration;
                newState.playbackCurrentTime = currentTime;
                return newState;
            })
        }
    };

    /**
     * Запускает процесс воспроизведения записанных действий пользователя
     */
    playRecorded = () => {
        this.enableProgressBar = true;
        const startTime = this.state.playbackCurrentTime < this.savedSnapshots.duration
            ? this.state.playbackCurrentTime
            : 0;
        this.setState({
            playing: true,
            playbackCurrentTime: startTime
        });
        this.savedSnapshots.addListener(this.playerCallBackFn);
        this.savedSnapshots.play();
    };

    /**
     * Слушатель событий на компонентах input формы
     * @param {Event} event событие на компоненте
     */
    inputListener = (event) => {
        const {name, value} = event.target;
        this.setState({
            [name]: { value }
        });
        if (this.state.recording) {
            this.recorder.addSnapshot({
                name,
                eventInfo: {
                    eventType: 'change',
                    value
                }
            });
        }
    };

    /**
     * Слушатель события Select на компонентах dropdown формы
     * @param {String} selected выбранное значение в выпадающем списке
     * @param {Event} event событие на компоненте
     * @param {String} name имя компонента (должно совпадать с именем переменной в state)
     */
    dropdownSelectListener = (selected, event, name) => {
        this.setState((prevState) => {
                return {
                    [name]: {
                        selected: selected,
                        open: true
                    }
                }
        });
        if (this.state.recording) {
            this.recorder.addSnapshot({
                name,
                eventInfo: {
                    eventType: 'select',
                    open: true,
                    selected: selected
                }
            });
        }
    };

    /**
     * Слушатель события Toggle на компонентах dropdown формы
     * @param {Boolean} open флаг видимости выпадающего списка
     * @param {Event} event событие на компоненте
     * @param {String} name имя компонента (должно совпадать с именем переменной в state)
     */
    dropdownToggleListener = (open, event, name) => {
        const dropdownPressed = [...this.GENDERS, '']
            .some(item => event.target.name === item);
        this.setState((prevState) => {
            return {
                [name]: {
                    selected: prevState[name].selected,
                    open: this.state.recording || dropdownPressed ? open : prevState[name].open,
                    hover: prevState[name].hover
                }
            }
        });
        if (this.state.recording) {
            this.recorder.addSnapshot({
                name,
                eventInfo: {
                    eventType: 'toggle',
                    open : open
                }
            });
        }
    };

    /**
     * Слушатель события MouseEnter на компонентах dropdown формы
     * @param {Event} event событие на компоненте
     * @param {String} name имя компонента (должно совпадать с именем переменной в state)
     */
    hoverListener = (event, name) => {
        if (this.state.recording) {
            this.recorder.addSnapshot({
                    name,
                    eventInfo: {
                        eventType: 'hover',
                        open: true,
                        hover: event.target.name
                    }
            });
        }
    };

    /**
     * Слушатель события Click на компоненте PlayerProgressBar
     * @param {number} time выбранное время с которого начинать воспроизведение
     */
    progressbarClickListener = (time) => {
        this.savedSnapshots.setPlayTime(time);
        this.setState({
            playbackCurrentTime: time
        });
    };

    /**
     * Слушатель события Select на компоненте Pills
     * @param {String} selected выбранный вариант
     */
    pillSelectListener = (selected) => {
        this.savedSnapshots.setSpeed(parseFloat(selected.slice(1)));
        this.setState({
            pillsSpeed: {selected}
        });
    };

    /**
     * Рендерит приложение
     */
    render() {
        const recordButton = (
            <Button label="Record"
                    type="record"
                    disabled={this.state.playing}
                    onClick={this.startRecording}
            />
        );
        const playButton = (
            <Button label="Play"
                    type="play"
                    disabled={this.state.recording || !this.savedSnapshots}
                    onClick={this.playRecorded}
            />
        );
        const stopButton = (
            <Button label="Stop"
                    type="stop"
                    onClick={this.stop}
            />
        );

        return (
            <div className="container text-center">
                <div className="form-horizontal" style={{padding: '20px 10px'}}>
                    <div className="form-group">
                        <label className="col-xs-2 control-label">Name</label>
                        <div className="col-xs-4">
                            <Input type="text"
                                   name="inputName"
                                   value={this.state.inputName.value}
                                   onChange={this.inputListener}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-xs-2 control-label">Second Name</label>
                        <div className="col-xs-4">
                            <Input type="text"
                                   name="inputSecondName"
                                   value={this.state.inputSecondName.value}
                                   onChange={this.inputListener}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-xs-2 control-label">Age</label>
                        <div className="col-xs-2">
                            <Input type="number"
                                   name="inputAge"
                                   value={this.state.inputAge.value}
                                   onChange={this.inputListener}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-xs-2 control-label">Gender</label>
                        <div className="col-xs-2 text-left">
                            <Dropdown id="dropdownGender"
                                      options={this.GENDERS}
                                      open={this.state.dropdownGender.open}
                                      selected={this.state.dropdownGender.selected}
                                      hover={this.state.dropdownGender.hover}
                                      onToggle={(open, event) => {
                                          this.dropdownToggleListener(open, event, 'dropdownGender')
                                      }}
                                      onSelect={(selected, event) => {
                                          this.dropdownSelectListener(selected, event, 'dropdownGender')
                                      }}
                                      onMouseEnter={(event) => {
                                          this.hoverListener(event, 'dropdownGender')
                                      }}
                            />
                        </div>
                    </div>
                    <hr/>
                    <div className="form-group">
                        <div className="col-xs-2 col-xs-offset-2 text-left">
                            {!this.state.recording ? recordButton : stopButton}
                        </div>
                        <div className="col-xs-2 text-right">
                            {!this.state.playing ? playButton : stopButton}
                        </div>
                    </div>
                    <div className={!this.savedSnapshots || this.state.recording ? 'hidden' : 'show'}>
                        <div className="form-group">
                            <div className="col-xs-4 col-xs-offset-2">
                                <PlayerProgressBar currentTime={this.state.playbackCurrentTime}
                                                   maxTime={this.savedSnapshots ? this.savedSnapshots.duration : 0}
                                                   disabled={!this.enableProgressBar}
                                                   onClick={(time) => {
                                                       this.progressbarClickListener(time)
                                                   }}
                                />
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="col-xs-2 control-label">Playback Speed:</label>
                            <div className="col-xs-4">
                                <Pills options={this.SPEEDS}
                                       selected={this.state.pillsSpeed.selected || this.SPEEDS[1]}
                                       onChange={(selected) => {
                                           this.pillSelectListener(selected)
                                       }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
