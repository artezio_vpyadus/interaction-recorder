import React from 'react';
import PropTypes from 'prop-types';

function PlayerProgressBar(props) {
    const startTimeArray = [];
    if (props.maxTime > 0) {
        const percent = Math.floor(props.maxTime / 100);
        for (let i = 0; i < 100; i++) {
            startTimeArray.push(i * percent);
        }
    }
    const renderProgressBlock = (blockStartTime) => {
        return (
            <div key={blockStartTime}
                 className="progress-bar"
                 style={{
                     width: '1%',
                     backgroundColor: blockStartTime >= props.currentTime ? 'transparent' : ''
                 }}
                 onClick={() => props.onClick(blockStartTime)}>
            </div>
        );
    };

    return (
        <div className="progress">
            {props.maxTime > 0 && !props.disabled? startTimeArray.map((startTime) => {
                return renderProgressBlock(startTime);
            }) : ''}
        </div>
    );
}

PlayerProgressBar.propTypes = {
    currentTime: PropTypes.number,
    maxTime: PropTypes.number,
    disabled: PropTypes.bool,
    onClick: PropTypes.func
};

PlayerProgressBar.defaultProps = {
    currentTime: 0,
    maxTime: 0,
    disabled: true,
    onClick: null
};

export default PlayerProgressBar;
