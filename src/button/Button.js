import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Glyphicon } from 'react-bootstrap';

function Button(props) {
    const btnClass = classNames({
        'btn': true,
        'btn-primary': props.type === 'record',
        'btn-danger': props.type === 'stop',
        'btn-success': props.type === 'play',
        'btn-sm': props.size === 'small',
        '': props.size === 'default',
        'btn-lg': props.size === 'large',
        'disabled': props.disabled
    });

    return (
        <button className={btnClass}
                name={props.type}
                onClick={!props.disabled ? props.onClick : null}
        >
            {props.label} <Glyphicon glyph={props.type} />
        </button>
    )
}

Button.propTypes = {
    label: PropTypes.string,
    type: PropTypes.string,
    size: PropTypes.string,
    disabled: PropTypes.bool,
    onClick: PropTypes.func
};

Button.defaultProps = {
    label: '',
    type: '',
    size: '',
    disabled: false,
    onClick: null
};

export default Button;
