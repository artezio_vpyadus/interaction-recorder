/** @module Компонент для работы с массивом снимков. */

/** Класс для работы с массивом снимков. */
class SnapshotList {
    constructor(...initialSnapshots) {
        this._snapshots = initialSnapshots || [];
    }

    /**
     * Добавляет в массив снимков снимок и время, когда он был сделан.
     * @param {Snapshot} snapshot снимок.
     * @param {number} delay время, когда был сделан снимок.
     */
    push(snapshot, delay) {
        this._snapshots.push({
            ...snapshot,
            delay
        });
    }

    /**
     * Возвращает объект итератора, для перебора массива снимков.
     * @return {SnapshotIterator}
     */
    iterator() {
        return new SnapshotIterator(this._snapshots);
    }
}

/** Класс, предоставляющий методы для перебора массива снимков. */
class SnapshotIterator {
    constructor(snapshots) {
        this._currentIndex = 0;
        this._snapshots = snapshots;
        this._currentFormState = null;
        this._prevTime = null;
    }

    /**
     * Возвращает снимки по порядку.
     * @return {Object.<boolean, Snapshot>} объект с 2 полями. done - флаг о наличии следующих элементов, value - текущий снимок.
     */
    next() {
        if (this._currentIndex < this._snapshots.length) {
            return {
                done: false,
                value: this._snapshots[this._currentIndex++]
            };
        } else {
            return {
                done: true,
                value: null
            };
        }
    }

    /**
     * Возващает актуальный снимок формы. Если такого нет, то возвращает <code>null</code>.
     * @param {number} time текущее время воспроизведения.
     * @return {Snapshot|null} актуальный снимок или <code>null</code>.
     */
     nextBy(time) {
         this._prevTime = time;
         if (this._currentIndex < this._snapshots.length && this._snapshots[this._currentIndex].delay <= time) {
             return this._snapshots[this._currentIndex++];
         } else {
             return null;
         }
     }

    /**
     * Возвращает массив актуальных снимков формы. Если таких нет, то возвращает null.
     * @param time текущее время воспроизведения.
     * @return {?Array.<Snapshot>} массив актуальных снимков формы.
     */
     moveTo(time) {
         let output = [];
         if (this._prevTime === time) {
             return null;
         }
         this._currentFormState = this._currentFormState || {};
         Object.keys(this._currentFormState).forEach(element => {
             this._currentFormState[element].isChanged = false;
         });
         for (let i = this._prevTime < time ? this._currentIndex + 1 : 0; i < this._snapshots.length; i++) {
             if (this._snapshots[i].delay <= time) {
                 this._currentIndex = i;
                 this._currentFormState[this._snapshots[i].name + this._snapshots[i].eventInfo.eventType] = {
                     snapshot: this._snapshots[i],
                     isChanged: true
                 };
             }
         }
        if (this._currentFormState) {
            Object.keys(this._currentFormState).forEach(element => {
                if (this._currentFormState[element].isChanged) {
                    output.push(this._currentFormState[element].snapshot);
                } else {
                    output.push(null);
                }
            });
        }
         this._prevTime = time;
         return output;
     }

}

export default SnapshotList;
