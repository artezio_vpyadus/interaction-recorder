/** @module Компонент для работы с записью. */

import Observable from "./Observable";

/** Класс для работы с записью. */
class Record extends Observable {
    /**
     * Создание записи.
     * @param {SnapshotList} snapshotList массив событий.
     * @param {number} duration длительность записи.
     */
    constructor(snapshotList, duration) {
        super();
        this._iterator = snapshotList.iterator();
        this._duration = duration;
        this._playSettings = {
            speed: 1,
            isPlaying: false,
        };
        this._startTime = null;
        this._currentTime = duration;
        this._delay = 42;
    }

    /**
     * Устанавливает значение скорости воспроизведения.
     * @param {number} speed скорость воспроизведения.
     */
    setSpeed(speed) {
        this._playSettings.speed = speed;
    }

    /**
     * Устанавливает с какого времени начнется воспроизведение.
     * @param {number} playTime время запуска воспроизведения.
     */
    setPlayTime(playTime) {
        this._currentTime = playTime;
        this._startTime = Date.now();
        this._iterator.moveTo(playTime).forEach(e => super._notifyListeners(e, this._currentTime));
    }

    /**
     * Возвращает длительность записи.
     * @returns {number} длительность записи в миллисекундах.
     */
    get duration() {
        return this._duration;
    }

    /**
     * Воспроизведение записи.
     */
    play() {
        if (this._currentTime >= this._duration) {
            this.setPlayTime(0);
        }
        this._playSettings.isPlaying = true;
        this._startTime = Date.now();
        let intervalId = setInterval(() => {
            if (!this._playSettings.isPlaying) {
                clearInterval(intervalId);
                return;
            }
            this._update();
            if (this._currentTime >= this._duration) {
                this._playSettings.isPlaying = false;
                clearInterval(intervalId);
            }
        }, this._delay);
    }

    /**
     * Остановка воспроизведения.
     */
    stop() {
        this._playSettings.isPlaying = false;
    }

    /**
     * Обновляет текущее состояние воспроизведения согласно текущему времени.
     * @private
     */
    _update() {
        this._currentTime += this._playSettings.speed * (Date.now() - this._startTime);
        this._startTime = Date.now();
        let currSnapshot = this._iterator.nextBy(this._currentTime);
        do {
            super._notifyListeners(currSnapshot, this._currentTime);
            currSnapshot = this._iterator.nextBy(this._currentTime);
        } while (currSnapshot !== null);
    }
}

export default Record;

/**
 * Этот коллбек отвечает за именения внешнего элемента и полосы прокрутки.
 * @callback Record~snapshotListener
 * @param {RecordState} snapshotState
 * @param {number} currentTime
 */

/**
 * @typedef {object} RecordState
 * @property {string|null} name
 * @property {object|null} eventInfo
 */

/**
 * @typedef {object} EventInfo
 * @property {string|null} eventType
 * @property {string|number} value
 */
