/** @module Компонент для записи действий на форме.  */
import Record from './Record';
import SnapshotList from './SnapshotList';

/** Класс для записи действий на форме. */
class Recorder {
    /** Создание записи. */
    constructor() {
        this.recordStartedAt = 0;
        this._snapshotList = new SnapshotList();
    }

    /**
     * Начало записи.
     * @param {...Snapshot} [initialSnapshots]
     */
    start(...initialSnapshots) {
        this.recordStartedAt = Date.now();
        initialSnapshots.forEach(initialSnapshot => {
            this.addSnapshot(initialSnapshot);
        });
    }

    /**
     * Сохранение изменений
     * @param {Snapshot} snapshot снимок состояния элемента на форме.
     */
    addSnapshot(snapshot) {
        this._snapshotList.push(snapshot, Date.now() - this.recordStartedAt);
    }

    /**
     * Остановка записи.
     * @returns {Record}
     */
    stop() {
        return new Record(this._snapshotList, Date.now() - this.recordStartedAt);
    }
}

export default Recorder;

/**
 * Снимок состояния элемента на форме.
 * @typedef {object} Snapshot
 * @property {string} name идентификатор элемента.
 * @property {EventInfo} eventInfo информация об изменении состояния.
 */
