/**@module Модуль для работы с подписчиками.*/

/**Класс для работы с подписчиками.*/
export default class Observable {
    constructor() {
        this._listeners = new Set();
    }

    /**
     * Добавления слушателя.
     * @param {function} listener коллбэк-функция, которая будет вызвана при оповещении.
     */
    addListener(listener) {
        this._listeners.add(listener);
    }

    /**
     * Удаление слушателя.
     * @param {function} listener коллбэк-функция, которая будет вызвана при оповещении.
     */
    removeListener(listener) {
        this._listeners.delete(listener);
    }

    /**
     * Оповещает всех слушателей, отдавая каждому все свои параметры.
     * @private
     */
    _notifyListeners() {
        this._listeners.forEach(listener => {
            listener(...arguments);
        })
    }
}